﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public static class Extensions
    {
        public static void SetDoubleBuffered(this Control c)
        {
            typeof(Control).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic, null, c, new object[] { true });
            foreach (Control sub in c.Controls) sub.SetDoubleBuffered();
        }

        public static void PreventUpdate(this Control c)
        {
            c.SuspendLayout();
            foreach (Control sub in c.Controls) sub.PreventUpdate();
        }

        public static void AllowUpdate(this Control c)
        {
            foreach (Control sub in c.Controls) sub.AllowUpdate();
            c.ResumeLayout();
        }
    }
}
