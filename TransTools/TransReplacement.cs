﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class TransReplacement : IComparable<TransReplacement>
    {
        public TransReplacement()
        {
            Category = string.Empty;
            Comment = string.Empty;
        }

        public long Position { get; set; }
        public byte[] Original { get; set; }
        public byte[] Replacement { get; set; }
        public string Category { get; set; }
        public string Comment { get; set; }

        public long Length
        {
            get
            {
                long olen = Original == null ? 0 : Original.LongLength;
                long rlen = Replacement == null ? 0 : Replacement.LongLength;
                return olen > rlen ? olen : rlen;
            }
        }

        public int CompareTo(TransReplacement other) => Position.CompareTo(other.Position);
    }
}
