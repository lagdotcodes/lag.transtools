﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    [Serializable]
    public class TransRom
    {
        public TransRom()
        {

        }
        public TransRom(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            FileName = filename;
            Size = fi.Length;
        }

        public string FileName { get; set; }
        public long Size { get; set; }
    }
}
