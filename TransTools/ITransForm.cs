﻿namespace TransTools
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public interface ITransForm
    {
        TransProj Project { get; set; }
        Form MdiParent { get; set; }
        FormStartPosition StartPosition { get; set; }
        FormBorderStyle FormBorderStyle { get; set; }
        FormWindowState WindowState { get; set; }
        string Text { get; set; }
        int Left { get; set; }
        int Top { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        Rectangle RestoreBounds { get; }

        event FormClosingEventHandler FormClosing;
        event EventHandler Move;
        event EventHandler Resize;

        void Show();
    }
}
