﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace TransTools
{
    public partial class BrowserForm : Form, ITransForm
    {
        private Label[] bytes;

        public BrowserForm()
        {
            InitializeComponent();

            for (int row = 0; row < 16; row++)
            {
                MainPanel.Controls.Add(RowLabel(row), 0, row + 1);
            }

            for (int col = 0; col < 16; col++)
            {
                MainPanel.Controls.Add(ColLabel(col), col + 1, 0);
            }

            bytes = new Label[256];
            for (int row = 0; row < 16; row++)
            {
                for (int col = 0; col < 16; col++)
                {
                    int i = row * 16 + col;
                    bytes[i] = ByteLabel();
                    MainPanel.Controls.Add(bytes[i], col + 1, row + 1);
                }
            }

            Offset = 0;
        }

        public TransProj Project { get; set; }
        public int Offset { get; set; }

        private Label ColLabel(int col)
        {
            Label label = new Label();
            label.Dock = DockStyle.Fill;
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Text = string.Format("x{0:X}", col);
            label.Font = new Font(label.Font, FontStyle.Bold);
            return label;
        }

        private Label RowLabel(int row)
        {
            Label label = new Label();
            label.Dock = DockStyle.Fill;
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Text = string.Format("{0:X}x", row);
            label.Font = new Font(label.Font, FontStyle.Bold);
            return label;
        }

        private Label ByteLabel()
        {
            Label label = new Label();
            label.Dock = DockStyle.Fill;
            label.TextAlign = ContentAlignment.MiddleCenter;
            return label;
        }

        public void ShowAll()
        {
            int max = (int)Project.Rom.Size - 256;
            if (Offset > max) Offset = max;
            if (Offset < 0) Offset = 0;

            OffsetBox.Text = string.Format("{0}", Offset);
            HexOffsetBox.Text = string.Format("{0:X}", Offset);

            Scroller.Minimum = 0;
            Scroller.Maximum = (int)Project.Rom.Size;
            Scroller.Value = Offset;

            using (BinaryReader reader = Project.Reader)
            {
                reader.BaseStream.Seek(Offset, SeekOrigin.Begin);
                for (int i = 0; i < 256; i++)
                {
                    byte[] b = reader.ReadBytes(1);
                    string s = Project.ToReadable(b);
                    bytes[i].Text = s;

                    bytes[i].BackColor = Project.IsOriginal(Offset + i) ? Color.White: Color.CornflowerBlue;
                }
            }
        }

        private void OffsetBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\r') return;

            int result;
            if (int.TryParse(OffsetBox.Text, out result))
            {
                e.Handled = true;
                Offset = result;
                ShowAll();
            }
        }

        private void Scroller_ValueChanged(object sender, EventArgs e)
        {
            Offset = Scroller.Value;
            ShowAll();
        }

        private void HexOffsetBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\r') return;

            int result;
            if (int.TryParse(HexOffsetBox.Text, NumberStyles.HexNumber, null, out result))
            {
                e.Handled = true;
                Offset = result;
                ShowAll();
            }
        }
    }
}
