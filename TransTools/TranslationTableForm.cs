﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class TranslationTableForm : Form, ITransForm
    {
        private Dictionary<byte, Label> labels;

        public TranslationTableForm()
        {
            InitializeComponent();

            labels = new Dictionary<byte, Label>();
            for (int a = 0; a < 256; a++)
            {
                Label lb = new Label();
                lb.BorderStyle = BorderStyle.Fixed3D;
                lb.Click += tableLabel_Click;
                lb.TextAlign = ContentAlignment.MiddleCenter;
                lb.BackColor = Color.Gray;
                Controls.Add(lb);
                labels[(byte)a] = lb;
            }
        }

        private void tableLabel_Click(object sender, EventArgs e)
        {
            Label lb = (Label)sender;
            byte a = 0;
            foreach (KeyValuePair<byte, Label> pair in labels)
            {
                if (pair.Value == lb)
                {
                    a = pair.Key;
                    break;
                }
            }

            TranslationTableDialog dlg = new TranslationTableDialog();
            dlg.Value = lb.Text;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                lb.Text = dlg.Value;

                if (string.IsNullOrEmpty(dlg.Value))
                {
                    Project.Translation.Remove(a);
                    lb.BackColor = Color.Gray;
                }
                else
                {
                    Project.Translation[a] = dlg.Value;
                    lb.BackColor = SystemColors.Control;
                }

                Project.Changed = true;
            }
        }

        public TransProj Project { get; set; }

        private void Arrange()
        {
            int height = ClientSize.Height / 16;
            int width = ClientSize.Width / 16;
            float fontsize = (width > height ? height : width) / 2;
            if (fontsize <= 0) return;
            Font font = new Font(Font.FontFamily, fontsize, FontStyle.Regular);

            for (int y = 0; y < 16; y++)
            {
                for (int x = 0; x < 16; x++)
                {
                    byte a = (byte)(x + y * 16);
                    Label lb = labels[a];
                    lb.Height = height;
                    lb.Width = width;
                    lb.Left = x * width;
                    lb.Top = y * height;
                    lb.Font = font;

                    if (Project.Translation.ContainsKey(a))
                    {
                        lb.Text = Project.Translation[a];
                        lb.BackColor = SystemColors.Control;
                    }
                }
            }
        }

        private void TranslationTableForm_Shown(object sender, EventArgs e)
        {
            Arrange();
        }

        private void TranslationTableForm_Resize(object sender, EventArgs e)
        {
            Arrange();
        }
    }
}
