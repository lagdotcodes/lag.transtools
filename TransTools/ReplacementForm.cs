﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    public partial class ReplacementForm : Form, ITransForm
    {
        private static readonly int RowHeight = 24;
        private string currentCategory;
        private TransReplacement[] activeReplacements;
        private List<Row> rows;

        public ReplacementForm()
        {
            currentCategory = string.Empty;
            rows = new List<Row>();
            InitializeComponent();

            ReplacementsPanel.HorizontalScroll.Enabled = false;
            ReplacementsPanel.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
            ReplacementsPanel.RowStyles.Clear();
        }

        public TransProj Project { get; set; }

        private void ShowAll()
        {
            this.PreventUpdate();

            CategoryCombo.Items.Clear();
            CategoryCombo.SelectedIndex = -1;
            IEnumerable<string> cats = (from cat in Project.Replacements
                                        orderby cat.Category
                                        select cat.Category).Distinct();
            foreach (string category in cats)
            {
                CategoryCombo.Items.Add(category);
                if (category == currentCategory) CategoryCombo.SelectedIndex = CategoryCombo.Items.Count - 1;
            }
            if (CategoryCombo.SelectedIndex == -1)
            {
                CategoryCombo.Items.Add(currentCategory);
                CategoryCombo.SelectedIndex = CategoryCombo.Items.Count - 1;
            }

            int oldCount = rows.Count;

            activeReplacements = Project.Replacements.Where(r => r.Category == currentCategory).ToArray();
            //ReplacementsPanel.Controls.Clear();

            int index = 0;
            foreach (TransReplacement rep in activeReplacements)
            {
                if (index >= oldCount)
                {
                    rows.Add(new Row(this, index));
                    ReplacementsPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, RowHeight));
                }
                else
                {
                    rows[index].Refresh();
                }

                index++;
            }

            // delete excess rows
            while (rows.Count > index)
            {
                rows[index].Dispose();
                rows.RemoveAt(index);
            }

            ReplacementsPanel.RowCount = activeReplacements.Length;
            ReplacementsPanel.Height = RowHeight * ReplacementsPanel.RowCount;

            this.AllowUpdate();
        }

        private void ReplaceButton_Click(object sender, EventArgs e)
        {
            byte[] image = File.ReadAllBytes(Project.Rom.FileName);
            foreach (TransReplacement rep in Project.Replacements)
            {
                rep.Replacement.CopyTo(image, rep.Position);
            }

            File.WriteAllBytes(Project.Rom.FileName, image);
            MessageBox.Show(string.Format("Made {0} replacements.", Project.Replacements.Count), "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RestoreButton_Click(object sender, EventArgs e)
        {
            byte[] image = File.ReadAllBytes(Project.Rom.FileName);
            foreach (TransReplacement rep in Project.Replacements)
            {
                rep.Original.CopyTo(image, rep.Position);
            }

            File.WriteAllBytes(Project.Rom.FileName, image);
            MessageBox.Show(string.Format("Restored {0} replacements.", Project.Replacements.Count), "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ReplacementForm_Shown(object sender, EventArgs e)
        {
            ShowAll();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            TransReplacement rep = new TransReplacement();
            rep.Original = new byte[0];
            rep.Replacement = new byte[0];
            rep.Category = currentCategory;
            Project.Replacements.Add(rep);

            Project.Changed = true;
            ShowAll();
            rows[rows.Count - 1].Offset.Focus();
        }

        private void ReadEmptyButton_Click(object sender, EventArgs e)
        {
            using (BinaryReader reader = Project.Reader)
            {
                foreach (TransReplacement rep in Project.Replacements)
                {
                    if (rep.Original.Length == 0)
                    {
                        reader.BaseStream.Seek(rep.Position, SeekOrigin.Begin);
                        rep.Original = reader.ReadBytes((int)rep.Length);
                    }
                }
            }

            Project.Changed = true;
            ShowAll();
        }

        private void CategoryCombo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            currentCategory = (string)CategoryCombo.SelectedItem;
            ShowAll();
        }

        private void CategoryCombo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\r') return;
            if (CategoryCombo.Items.Contains(CategoryCombo.Text)) return;

            currentCategory = CategoryCombo.Text;
            AddButton_Click(this, null);
        }

        private class Row : IDisposable
        {
            private static readonly Color Normal = Color.White;
            private static readonly Color Success = Color.LightGreen;
            private static readonly Color Failure = Color.PaleVioletRed;

            public Row(ReplacementForm parent, int index)
            {
                Parent = parent;
                Index = index;

                Offset = OffsetBox();
                Original = OriginalBox();
                Replacement = ReplacementBox();
                Category = CategoryBox();
                Comment = CommentBox();

                parent.ReplacementsPanel.Controls.Add(Offset, 0, index);
                parent.ReplacementsPanel.Controls.Add(Original, 1, index);
                parent.ReplacementsPanel.Controls.Add(Replacement, 2, index);
                parent.ReplacementsPanel.Controls.Add(Comment, 3, index);
                parent.ReplacementsPanel.Controls.Add(Category, 4, index);
            }

            public int Index { get; }
            public TextBox Offset { get; }
            public TextBox Original { get; }
            public TextBox Replacement { get; }
            public TextBox Category { get; }
            public TextBox Comment { get; }

            private ReplacementForm Parent { get; }
            private TransProj Project => Parent.Project;
            private TransReplacement Reference => Parent.activeReplacements[Index];

            public void Refresh()
            {
                Offset.Text = Reference.Position.ToString();
                Original.Text = Project.ToReadable(Reference.Original);
                Replacement.Text = Project.ToReadable(Reference.Replacement);
                Category.Text = Reference.Category;
                Comment.Text = Reference.Comment;

                Offset.BackColor = Normal;
                Original.BackColor = Normal;
                Replacement.BackColor = Normal;
                Category.BackColor = Normal;
                Comment.BackColor = Normal;
            }

            private TextBox MakeBox()
            {
                TextBox box = new TextBox();
                box.Dock = DockStyle.Fill;
                return box;
            }

            private TextBox OffsetBox()
            {
                TextBox box = MakeBox();
                box.Text = Reference.Position.ToString();
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    long result;
                    if (long.TryParse(box.Text, out result))
                    {
                        Reference.Position = result;
                        box.BackColor = Success;
                        Project.Changed = true;
                    }
                    else
                    {
                        System.Media.SystemSounds.Beep.Play();
                        box.Text = Reference.Position.ToString();
                        box.BackColor = Failure;
                    }

                    e.Handled = true;
                };

                return box;
            }

            private TextBox OriginalBox()
            {
                TextBox box = MakeBox();
                string converted = Project.ToReadable(Reference.Original);
                box.Text = converted;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    string collector;
                    byte[] bytes = Project.ToRom(box.Text, out collector);
                    string conv = Project.ToReadable(bytes);
                    box.Text = conv;

                    if (collector.Length > 0)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        box.BackColor = Failure;
                    }
                    else
                    {
                        box.BackColor = Success;
                        Reference.Original = bytes;
                        Project.Changed = true;
                    }

                    e.Handled = true;
                };

                return box;
            }

            private TextBox ReplacementBox()
            {
                TextBox box = MakeBox();
                string converted = Project.ToReadable(Reference.Replacement);
                box.Text = converted;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    string collector;
                    byte[] bytes = Project.ToRom(box.Text, out collector);
                    string conv = Project.ToReadable(bytes);
                    box.Text = conv;

                    if (collector.Length > 0)
                    {
                        System.Media.SystemSounds.Beep.Play();
                        box.BackColor = Failure;
                    }
                    else
                    {
                        box.BackColor = Success;
                        Reference.Replacement = bytes;
                        Project.Changed = true;
                    }

                    e.Handled = true;
                };

                return box;
            }

            private TextBox CategoryBox()
            {
                TextBox box = MakeBox();
                box.Font = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Regular);
                box.Text = Reference.Category;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    box.BackColor = Success;
                    Reference.Category = box.Text;
                    Project.Changed = true;

                    e.Handled = true;
                };

                return box;
            }

            private TextBox CommentBox()
            {
                TextBox box = MakeBox();
                box.Font = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Regular);
                box.Text = Reference.Comment;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    box.BackColor = Success;
                    Reference.Comment = box.Text;
                    Project.Changed = true;

                    e.Handled = true;
                };

                return box;
            }

            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        Offset.Dispose();
                        Original.Dispose();
                        Replacement.Dispose();
                        Category.Dispose();
                        Comment.Dispose();
                    }

                    disposedValue = true;
                }
            }

            public void Dispose()
            {
                Dispose(true);
            }
            #endregion
        }
    }
}
