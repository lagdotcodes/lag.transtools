﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class ParentForm : Form
    {
        public TransProj Project { get; set; }
        public string ProjectFileName { get; set; }
        public bool ProjectNeedsSaving { get; set; }
        public bool ProjectClosing { get; set; }

        public ParentForm()
        {
            InitializeComponent();
        }

        protected void ProjectIsLoaded(bool v)
        {
            closeToolStripMenuItem.Enabled = v;
            saveToolStripMenuItem.Enabled = v;
            saveAsToolStripMenuItem.Enabled = v;
            exportAsCSVToolStripMenuItem.Enabled = v;

            if (v)
            {
                if (string.IsNullOrEmpty(ProjectFileName)) Text = "Lag.TransTools - unsaved";
                else Text = "Lag.TransTools - " + Path.GetFileName(ProjectFileName);
            }
            else Text = "Lag.TransTools";
        }

        protected bool NeedsSaving()
        {
            if (!ProjectNeedsSaving) return true;
            DialogResult result = MessageBox.Show("Save the current project?", "Save?", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Cancel) return false;
            if (result == DialogResult.Yes) return SaveProject();
            return true;
        }

        protected bool SaveProject()
        {
            if (string.IsNullOrEmpty(ProjectFileName))
            {
                if (saveProjectDlg.ShowDialog() == DialogResult.OK)
                {
                    SaveProject(saveProjectDlg.FileName);
                    return true;
                }

                return false;
            }

            SaveProject(ProjectFileName);
            return true;
        }

        protected void SaveProject(string filename)
        {
            Project.Save(filename);
            ProjectFileName = filename;
            ProjectNeedsSaving = false;
            ProjectIsLoaded(true);
        }

        protected void LoadProject(string filename)
        {
            Project = TransProj.Load(filename);
            ProjectFileName = filename;
            ProjectNeedsSaving = false;
            ProjectIsLoaded(true);
        }

        protected void CloseProject()
        {
            ProjectClosing = true;
            foreach (Form form in MdiChildren)
            {
                form.Close();
            }

            Project = null;
            ProjectFileName = string.Empty;
            ProjectNeedsSaving = false;
            ProjectIsLoaded(false);
            ProjectClosing = false;
        }

        protected void InitProject()
        {
            Project.OnChange += Project_OnChange;
            ChildForm(new TranslationTableForm());
            ChildForm(new SearchForm());
            ChildForm(new ReplacementForm());
            ChildForm(new BrowserForm());
            ChildForm(new VarForm());
        }

        private void Project_OnChange(TransProj project)
        {
            ProjectNeedsSaving = true;
            Text += "*";
        }

        protected void ChildForm(ITransForm form)
        {
            form.Project = Project;
            form.MdiParent = this;

            if (Project.Windows.ContainsKey(form.Text))
            {
                TransWin win = Project.Windows[form.Text];
                form.StartPosition = FormStartPosition.Manual;
                win.Apply(form);
            }

            form.Show();
            form.Move += childForm_Move;
            form.Resize += childForm_Resize;
            form.FormClosing += childForm_FormClosing;
        }

        private void childForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && !ProjectClosing)
            {
                ITransForm form = (ITransForm)sender;
                e.Cancel = true;
                form.WindowState = FormWindowState.Minimized;
            }
        }

        private void childForm_Resize(object sender, EventArgs e)
        {
            ITransForm form = (ITransForm)sender;
            Project.Windows[form.Text] = new TransWin(form);
            Project.Changed = true;
        }

        private void childForm_Move(object sender, EventArgs e)
        {
            ITransForm form = (ITransForm)sender;
            Project.Windows[form.Text] = new TransWin(form);
            Project.Changed = true;
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!NeedsSaving()) return;
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!NeedsSaving()) return;

            if (openRomDlg.ShowDialog() == DialogResult.OK)
            {
                Project = new TransProj();
                Project.LoadRom(openRomDlg.FileName);
                ProjectFileName = string.Empty;
                ProjectNeedsSaving = true;
                ProjectIsLoaded(true);
                InitProject();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!NeedsSaving()) return;

            if (openProjectDlg.ShowDialog() == DialogResult.OK)
            {
                LoadProject(openProjectDlg.FileName);
                InitProject();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveProject();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveProjectDlg.ShowDialog() == DialogResult.OK)
            {
                SaveProject(saveProjectDlg.FileName);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!NeedsSaving()) return;
            CloseProject();
        }

        private void ParentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!NeedsSaving())
            {
                e.Cancel = true;
                return;
            }
        }

        private void arrangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void tileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void exportAsCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("TranslationExport.csv", false, Encoding.UTF8))
            {
                writer.WriteLine("Offset,Length,Category,Original,Replacement,Comment");
                foreach (TransReplacement rep in Project.Replacements)
                {
                    string original = Project.ToReadable(rep.Original);
                    string replacement = Project.ToReadable(rep.Replacement);
                    writer.WriteLine($"{rep.Position},{rep.Length},{rep.Category},{original},{replacement},{rep.Comment}");
                }

                string filename = (writer.BaseStream as FileStream).Name;
                MessageBox.Show($"Wrote {filename}.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
