﻿namespace TransTools
{
    partial class VarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandsBox = new System.Windows.Forms.GroupBox();
            this.VariablesPanel = new System.Windows.Forms.TableLayoutPanel();
            this.AddButton = new System.Windows.Forms.Button();
            this.CommandsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandsBox
            // 
            this.CommandsBox.Controls.Add(this.AddButton);
            this.CommandsBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandsBox.Location = new System.Drawing.Point(0, 0);
            this.CommandsBox.Name = "CommandsBox";
            this.CommandsBox.Size = new System.Drawing.Size(292, 53);
            this.CommandsBox.TabIndex = 3;
            this.CommandsBox.TabStop = false;
            this.CommandsBox.Text = "Commands";
            // 
            // VariablesPanel
            // 
            this.VariablesPanel.ColumnCount = 3;
            this.VariablesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.VariablesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.VariablesPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.VariablesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VariablesPanel.Location = new System.Drawing.Point(0, 53);
            this.VariablesPanel.Name = "VariablesPanel";
            this.VariablesPanel.RowCount = 1;
            this.VariablesPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.VariablesPanel.Size = new System.Drawing.Size(292, 220);
            this.VariablesPanel.TabIndex = 4;
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(12, 19);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 3;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // VarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.VariablesPanel);
            this.Controls.Add(this.CommandsBox);
            this.Name = "VarForm";
            this.Text = "Variables";
            this.Shown += new System.EventHandler(this.VarForm_Shown);
            this.CommandsBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox CommandsBox;
        private System.Windows.Forms.TableLayoutPanel VariablesPanel;
        private System.Windows.Forms.Button AddButton;
    }
}