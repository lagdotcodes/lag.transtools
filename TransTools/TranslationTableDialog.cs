﻿namespace TransTools
{
    using System;
    using System.Windows.Forms;

    public partial class TranslationTableDialog : Form
    {
        public TranslationTableDialog()
        {
            InitializeComponent();
        }

        public string Value
        {
            get { return ValueBox.Text; }
            set { ValueBox.Text = value; }
        }

        private void TranslationTableDialog_Shown(object sender, EventArgs e)
        {
            ValueBox.SelectAll();
            ValueBox.Focus();
        }

        private void ValueBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                DialogResult = DialogResult.OK;
                e.SuppressKeyPress = true;
            }
        }
    }
}
