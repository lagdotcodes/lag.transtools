﻿namespace TransTools
{
    partial class ReplacementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandsBox = new System.Windows.Forms.GroupBox();
            this.CategoryCombo = new System.Windows.Forms.ComboBox();
            this.ReadEmptyButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.RestoreButton = new System.Windows.Forms.Button();
            this.ReplaceButton = new System.Windows.Forms.Button();
            this.ReplacementsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CommandsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandsBox
            // 
            this.CommandsBox.Controls.Add(this.CategoryCombo);
            this.CommandsBox.Controls.Add(this.ReadEmptyButton);
            this.CommandsBox.Controls.Add(this.AddButton);
            this.CommandsBox.Controls.Add(this.RestoreButton);
            this.CommandsBox.Controls.Add(this.ReplaceButton);
            this.CommandsBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommandsBox.Location = new System.Drawing.Point(0, 0);
            this.CommandsBox.Name = "CommandsBox";
            this.CommandsBox.Size = new System.Drawing.Size(699, 53);
            this.CommandsBox.TabIndex = 0;
            this.CommandsBox.TabStop = false;
            this.CommandsBox.Text = "Commands";
            // 
            // CategoryCombo
            // 
            this.CategoryCombo.FormattingEnabled = true;
            this.CategoryCombo.Location = new System.Drawing.Point(6, 21);
            this.CategoryCombo.Name = "CategoryCombo";
            this.CategoryCombo.Size = new System.Drawing.Size(113, 21);
            this.CategoryCombo.TabIndex = 4;
            this.CategoryCombo.SelectionChangeCommitted += new System.EventHandler(this.CategoryCombo_SelectionChangeCommitted);
            this.CategoryCombo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CategoryCombo_KeyPress);
            // 
            // ReadEmptyButton
            // 
            this.ReadEmptyButton.Location = new System.Drawing.Point(287, 19);
            this.ReadEmptyButton.Name = "ReadEmptyButton";
            this.ReadEmptyButton.Size = new System.Drawing.Size(75, 23);
            this.ReadEmptyButton.TabIndex = 3;
            this.ReadEmptyButton.Text = "Read Empty";
            this.ReadEmptyButton.UseVisualStyleBackColor = true;
            this.ReadEmptyButton.Click += new System.EventHandler(this.ReadEmptyButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(368, 19);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 2;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // RestoreButton
            // 
            this.RestoreButton.Location = new System.Drawing.Point(206, 19);
            this.RestoreButton.Name = "RestoreButton";
            this.RestoreButton.Size = new System.Drawing.Size(75, 23);
            this.RestoreButton.TabIndex = 1;
            this.RestoreButton.Text = "Restore";
            this.RestoreButton.UseVisualStyleBackColor = true;
            this.RestoreButton.Click += new System.EventHandler(this.RestoreButton_Click);
            // 
            // ReplaceButton
            // 
            this.ReplaceButton.Location = new System.Drawing.Point(125, 19);
            this.ReplaceButton.Name = "ReplaceButton";
            this.ReplaceButton.Size = new System.Drawing.Size(75, 23);
            this.ReplaceButton.TabIndex = 0;
            this.ReplaceButton.Text = "Replace";
            this.ReplaceButton.UseVisualStyleBackColor = true;
            this.ReplaceButton.Click += new System.EventHandler(this.ReplaceButton_Click);
            // 
            // ReplacementsPanel
            // 
            this.ReplacementsPanel.AutoScroll = true;
            this.ReplacementsPanel.AutoSize = true;
            this.ReplacementsPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ReplacementsPanel.ColumnCount = 5;
            this.ReplacementsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.ReplacementsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ReplacementsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ReplacementsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ReplacementsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.ReplacementsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReplacementsPanel.Location = new System.Drawing.Point(0, 53);
            this.ReplacementsPanel.Name = "ReplacementsPanel";
            this.ReplacementsPanel.RowCount = 1;
            this.ReplacementsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 309F));
            this.ReplacementsPanel.Size = new System.Drawing.Size(699, 309);
            this.ReplacementsPanel.TabIndex = 1;
            // 
            // ReplacementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 362);
            this.Controls.Add(this.ReplacementsPanel);
            this.Controls.Add(this.CommandsBox);
            this.Name = "ReplacementForm";
            this.Text = "Replacements";
            this.Shown += new System.EventHandler(this.ReplacementForm_Shown);
            this.CommandsBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox CommandsBox;
        private System.Windows.Forms.Button RestoreButton;
        private System.Windows.Forms.Button ReplaceButton;
        private System.Windows.Forms.TableLayoutPanel ReplacementsPanel;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button ReadEmptyButton;
        private System.Windows.Forms.ComboBox CategoryCombo;
    }
}