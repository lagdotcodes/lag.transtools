﻿namespace TransTools
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    [Serializable]
    public class TransWin
    {
        public TransWin() { }
        public TransWin(ITransForm form)
        {
            WindowState = form.WindowState;

            if (form.WindowState == FormWindowState.Normal)
            {
                Left = form.Left;
                Top = form.Top;
                Width = form.Width;
                Height = form.Height;
            }
            else
            {
                Left = form.RestoreBounds.Left;
                Top = form.RestoreBounds.Top;
                Width = form.RestoreBounds.Width;
                Height = form.RestoreBounds.Height;
            }
        }

        public void Apply(ITransForm form)
        {
            form.Left = Left;
            form.Top = Top;
            form.Width = Width;
            form.Height = Height;
            form.WindowState = WindowState;
        }

        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public FormWindowState WindowState { get; set; }
    }
}
