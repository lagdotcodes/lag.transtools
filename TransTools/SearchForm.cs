﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class SearchForm : Form, ITransForm
    {
        private Action<long> addResult;
        private Action finishSearch;

        public SearchForm()
        {
            InitializeComponent();
            addResult = new Action<long>(AddResult);
            finishSearch = new Action(FinishSearch);
        }

        private void FinishSearch()
        {
            SearchBox.ReadOnly = false;
            Cursor = Cursors.Default;
        }

        private void AddResult(long offset)
        {
            ResultsList.Invoke(new Action(() => ResultsList.Items.Add(offset)));
        }

        public TransProj Project { get; set; }

        private void SearchBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                SearchBox.ReadOnly = true;
                string search = SearchBox.Text;
                byte[] bytes = ToRom(search);
                ResultsList.Items.Clear();
                Cursor = Cursors.WaitCursor;
                e.Handled = true;

                Task.Run(() => Search(bytes));
            }
        }

        private void Search(byte[] bytes)
        {
            if (bytes.Length > 0)
            {
                List<byte> collector = new List<byte>();
                using (BinaryReader reader = Project.Reader)
                {
                    collector.AddRange(reader.ReadBytes(bytes.Length));
                    while (reader.BaseStream.Position < reader.BaseStream.Length)
                    {
                        if (bytes.SequenceEqual(collector))
                        {
                            long pos = reader.BaseStream.Position - bytes.Length;
                            Invoke(addResult, new object[]{ pos });
                        }

                        collector.RemoveAt(0);
                        collector.Add(reader.ReadByte());
                    }
                }
            }

            Invoke(finishSearch);
        }

        private byte[] ToRom(string o)
        {
            string collector;
            byte[] bytes = Project.ToRom(o, out collector);
            string converted = BitConverter.ToString(bytes, 0);
            TranslatedBox.Text = string.IsNullOrEmpty(collector) ? converted : string.Format("{0} :{1}", converted, collector);
            return bytes;
        }

        private void SearchForm_Resize(object sender, EventArgs e)
        {
            SearchBox.Width = Width - 35;
            TranslatedBox.Width = SearchBox.Width;
        }
    }
}
