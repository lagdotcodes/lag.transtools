﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class VarForm : Form, ITransForm
    {
        private static readonly int RowHeight = 24;
        private List<Row> rows;

        public VarForm()
        {
            rows = new List<Row>();
            InitializeComponent();

            VariablesPanel.HorizontalScroll.Enabled = false;
            VariablesPanel.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
            VariablesPanel.RowStyles.Clear();
        }

        public TransProj Project { get; set; }

        private void ShowAll()
        {
            this.PreventUpdate();

            int oldCount = rows.Count;

            int index = 0;
            foreach (TransVar var in Project.Variables)
            {
                if (index >= oldCount)
                {
                    rows.Add(new Row(this, index));
                    VariablesPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, RowHeight));
                }
                else
                {
                    rows[index].Refresh();
                }

                index++;
            }

            // delete excess rows
            while (rows.Count > index)
            {
                rows[index].Dispose();
                rows.RemoveAt(index);
            }

            VariablesPanel.RowCount = Project.Variables.Count;
            VariablesPanel.Height = RowHeight * VariablesPanel.RowCount;

            this.AllowUpdate();
        }

        private class Row : IDisposable
        {
            private static readonly Color Normal = Color.White;
            private static readonly Color Success = Color.LightGreen;
            private static readonly Color Failure = Color.PaleVioletRed;

            public Row(VarForm parent, int index)
            {
                Parent = parent;
                Index = index;

                Sequence = SequenceBox();
                Name = NameBox();
                Comment = CommentBox();

                parent.VariablesPanel.Controls.Add(Sequence, 0, index);
                parent.VariablesPanel.Controls.Add(Name, 1, index);
                parent.VariablesPanel.Controls.Add(Comment, 2, index);
            }

            public int Index { get; }
            public TextBox Sequence { get; }
            public TextBox Name { get; }
            public TextBox Comment { get; }

            private VarForm Parent { get; }
            private TransProj Project => Parent.Project;
            private TransVar Reference => Parent.Project.Variables[Index];

            public void Refresh()
            {
                Sequence.Text = Reference.Sequence;
                Name.Text = Reference.Name;
                Comment.Text = Reference.Comment;

                Sequence.BackColor = Normal;
                Name.BackColor = Normal;
                Comment.BackColor = Normal;
            }

            private TextBox MakeBox()
            {
                TextBox box = new TextBox();
                box.Dock = DockStyle.Fill;
                return box;
            }

            private TextBox SequenceBox()
            {
                TextBox box = MakeBox();
                string converted = Reference.Sequence;
                box.Text = converted;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    box.BackColor = Success;
                    Reference.Sequence = box.Text;
                    e.Handled = true;
                };

                return box;
            }

            private TextBox NameBox()
            {
                TextBox box = MakeBox();
                string converted = Reference.Name;
                box.Text = converted;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    box.BackColor = Success;
                    Reference.Name = box.Text;
                    e.Handled = true;
                };

                return box;
            }
            
            private TextBox CommentBox()
            {
                TextBox box = MakeBox();
                box.Font = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Regular);
                box.Text = Reference.Comment;
                box.KeyPress += (sender, e) =>
                {
                    if (e.KeyChar != '\r') return;

                    box.BackColor = Success;
                    Reference.Comment = box.Text;
                    Project.Changed = true;

                    e.Handled = true;
                };

                return box;
            }

            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        Sequence.Dispose();
                        Name.Dispose();
                        Comment.Dispose();
                    }

                    disposedValue = true;
                }
            }

            public void Dispose()
            {
                Dispose(true);
            }
            #endregion
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            TransVar var = new TransVar();
            Project.Variables.Add(var);

            Project.Changed = true;
            ShowAll();
            rows[rows.Count - 1].Sequence.Focus();
        }

        private void VarForm_Shown(object sender, EventArgs e)
        {
            ShowAll();
        }
    }
}
