﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [Serializable]
    public class TransVar : IComparable<TransVar>
    {
        public TransVar()
        {
            Sequence = string.Empty;
            Name = string.Empty;
            Comment = string.Empty;
        }

        public string Sequence { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }

        public int CompareTo(TransVar other) => Name.CompareTo(other.Name);
    }
}
