﻿namespace TransTools
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;

    [Serializable]
    public class TransProj
    {
        private bool changed;

        public TransProj()
        {
            Translation = new SerializableDictionary<byte, string>();
            Notes = new SerializableDictionary<string, string>();
            Windows = new SerializableDictionary<string, TransWin>();
            Replacements = new List<TransReplacement>();
            Variables = new List<TransVar>();
        }

        public static TransProj Load(string filename)
        {
            using (StreamReader stream = new StreamReader(filename))
            {
                XmlSerializer reader = new XmlSerializer(typeof(TransProj));
                return (TransProj)reader.Deserialize(stream);
            }
        }

        public TransRom Rom { get; set; }
        public SerializableDictionary<byte, string> Translation { get; set; }
        public SerializableDictionary<string, string> Notes { get; set; }
        public SerializableDictionary<string, TransWin> Windows { get; set; }
        public List<TransReplacement> Replacements { get; set; }
        public List<TransVar> Variables { get; set; }

        [XmlIgnore]
        public bool Changed
        {
            get { return changed; }
            set
            {
                if (!changed && value)
                {
                    if (OnChange != null) OnChange(this);
                }

                changed = value;
            }
        }
        public delegate void TransProjEvent(TransProj project);
        public event TransProjEvent OnChange;

        [XmlIgnore]
        public BinaryReader Reader => new BinaryReader(File.OpenRead(Rom.FileName));

        public void Save(string filename)
        {
            Cleanup();

            using (StreamWriter stream = new StreamWriter(filename))
            {
                XmlSerializer writer = new XmlSerializer(typeof(TransProj));
                writer.Serialize(stream, this);
            }

            changed = false;
        }

        public void Cleanup()
        {
            Replacements.RemoveAll(r => r.Position == 0);
            Replacements.Sort();
        }

        public void LoadRom(string filename)
        {
            Rom = new TransRom(filename);
        }

        public bool IsOriginal(int offset)
        {
            foreach (TransReplacement rep in Replacements)
            {
                if (rep.Position <= offset && (rep.Position + rep.Length) > offset) return false;
            }

            return true;
        }

        public byte[] ToRom(string o, out string collector)
        {
            string withVars = o;
            foreach (TransVar var in Variables)
            {
                withVars = withVars.Replace(var.Name, var.Sequence);
            }

            List<byte> trans = new List<byte>();
            collector = string.Empty;
            foreach (char c in withVars)
            {
                collector += c;
                foreach (KeyValuePair<byte, string> pair in Translation)
                {
                    if (pair.Value == collector)
                    {
                        collector = string.Empty;
                        trans.Add(pair.Key);
                        break;
                    }
                }
            }

            return trans.ToArray();
        }

        public string ToReadable(byte[] bytes)
        {
            string s = string.Empty;
            foreach (byte b in bytes)
            {
                if (Translation.ContainsKey(b)) s += Translation[b];
                else s += '?';
            }

            foreach (TransVar var in Variables)
            {
                s = s.Replace(var.Sequence, var.Name);
            }

            return s;
        }
    }
}
